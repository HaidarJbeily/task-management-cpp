#ifndef MAINAPP_H
#define MAINAPP_H

#include <QDialog>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSql>
#include <QMessageBox>
#include "appwindow.h"
namespace Ui {
class MainApp;
}

class MainApp : public QDialog
{
    Q_OBJECT

public:
    explicit MainApp(QWidget *parent = nullptr);
    ~MainApp();


private slots:
    void on_MainLoginPushButton_clicked();

    void on_MainRegisterPushButton_clicked();

    void on_cancel_clicked();

    void on_cancelReg_clicked();

    void on_Ok_clicked();

    void on_OkReg_clicked();

private:
    Ui::MainApp *ui;
};

#endif // MAINAPP_H
