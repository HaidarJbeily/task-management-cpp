#include "mainapp.h"
#include "ui_mainapp.h"

MainApp::MainApp(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MainApp)
{

    ui->setupUi(this);
   ui->groupBoxLogin->hide();
   ui->groupBoxRegister->hide();

}

MainApp::~MainApp()
{
    delete ui;
}

void MainApp::on_MainLoginPushButton_clicked()
{
    ui->groupBoxLogin->show();
}

void MainApp::on_MainRegisterPushButton_clicked()
{
    ui->groupBoxRegister->show();
}


void MainApp::on_cancel_clicked()
{
    ui->groupBoxLogin->hide();
}

void MainApp::on_cancelReg_clicked()
{
     ui->groupBoxRegister->hide();
}

void MainApp::on_Ok_clicked()
{
    QString username = ui->usernameLoginText->text();
    QString password = ui->passwordLoginText->text();
    QSqlDatabase   database  =  QSqlDatabase::addDatabase("QSQLITE");
    database.setDatabaseName("C:\\Users\\Windows.10\\Documents\\TaskManagement\\user.db");
    if (database.open())
    {
        QSqlQuery query(database);
        query.prepare(QString("SELECT usrname,password FROM userdb WHERE usrname = :username"));
        query.bindValue(":username",username);


        if (query.exec())
        {
                int chkuser = 0, chkpass = 0;

                while (query.next()) {
                     QString usernameFromDB = query.value(0).toString();
                     QString passwordFromDB = query.value(1).toString();
                     if (username == usernameFromDB)
                     {
                         chkuser=1;
                         if(password == passwordFromDB)
                         {
                             chkpass=1;
                             break;
                         }

                     }
                }


                if (chkuser)
                {
                    if (chkpass)
                    {
                        AppWindow *frm = new AppWindow(this,username);
                           frm->username = username;
                        frm->show();
                        this->hide();
                        database.close();
                    }
                    else
                    {
                    QMessageBox::information(this,"Error!","Password is Not Correct");
                    }

                }
                else
                {
                    QMessageBox::information(this,"Error!","Username is Not Correct");
                }

        }
        else
        {
            QMessageBox::information(this,"Error!","System Problem");
        }
       }
    else
    {
        QMessageBox::information(this,"Error!","DataBase Problem");
    }

}

void MainApp::on_OkReg_clicked()
{
    QString Password = ui->passwordRegisterTxt->text();
    QString rePassword = ui->reapeatPassword->text();
    QString username = ui->userNameRegisterTxt->text();
    QString Email = ui->emailRegisterTxt->text();
    QString Fullname = ui->fullNameRegisterTxt->text();
    if (Password == rePassword){
        QSqlDatabase   database  =  QSqlDatabase::addDatabase("QSQLITE");
        database.setDatabaseName("C:\\Users\\Windows.10\\Documents\\TaskManagement\\user.db");
        if (database.open())
        {
            QSqlQuery query(database);
            query.prepare(QString("SELECT usrname FROM userdb WHERE usrname = :username"));
            query.bindValue(":username",username);


            if (query.exec())
            {
                    int chk = 0;

                    while (query.next()) {
                         QString usernameFromDB = query.value(0).toString();
                         if (username == usernameFromDB)
                         {
                             chk=1;
                             break;
                         }
                    }


                    if (chk)
                    {
                        QMessageBox::information(this,"Error!"," This username had been used before. \nPlease re-Enter with new one.");
                    }
                    else
                    {
                        query.prepare("INSERT INTO userdb (usrname,password,fullname,email) VALUES (:username,:Password,:Fullname,:Email)");
                        query.bindValue(":username",username);
                        query.bindValue(":Password",Password);
                        query.bindValue(":Fullname",Fullname);
                        query.bindValue(":Email",Email);
                        if(query.exec())
                        {
                            QMessageBox::information(this,"Congrats!","Registeration Completed!");
                            ui->groupBoxRegister->hide();
                            database.close();
                        }
                        else
                        {
                            QMessageBox::information(this,"Sorry!","Registeration Failed!");
                        }
                    }

            }
            else
            {
                QMessageBox::information(this,"Query Failed","Failed to excute Query");
            }
        }
    }
    else
    {
        QMessageBox::information(this,"","Please Make sure of the password again");
    }
}
