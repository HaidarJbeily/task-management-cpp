#include "mainapp.h"
#include "appwindow.h"
#include "ui_appwindow.h"
#include <QMessageBox>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSql>
#include <stdio.h>
AppWindow::AppWindow(QWidget *parent,QString UserName ) :
    QMainWindow(parent),
    ui(new Ui::AppWindow)
{


    ui->setupUi(this);

    ui->addTaskLabel->hide();
    ui->lineEditAddTask->hide();
    ui->pushButtonCancelAdd->hide();
    ui->pushButtonVerFaDD->hide();
    QString username = UserName;
    QSqlDatabase   database  =  QSqlDatabase::addDatabase("QSQLITE");
    database.setDatabaseName("C:\\Users\\Windows.10\\Documents\\TaskManagement\\user.db");

    if (database.open())
    {
        QSqlQuery query(database);
        query.prepare(QString("SELECT Todo,Inprogress ,done FROM userdb WHERE usrname = :username"));
        query.bindValue(":username",username);
        if (query.exec())
        {
            QString indx1;
            QString indx2;
            QString indx3 ;
            while(query.next())
               {
              indx1 = query.value(0).toString();
              indx2 = query.value(1).toString();
              indx3 = query.value(2).toString();
            }
             QStringList todo =  indx1.split("&7&",QString::SkipEmptyParts);
             QStringList inprogress =  indx2.split("&7&",QString::SkipEmptyParts);
             QStringList done =  indx3.split("&7&",QString::SkipEmptyParts);
             for (int i =0;i<todo.size() ;i++ )
                 ui->toDoList->addItem(todo.at(i).toLocal8Bit().constData());


             for (int i =0;i<inprogress.size() ;i++ )
                 ui->inProgressList->addItem(inprogress.at(i).toLocal8Bit().constData());


             for (int i =0;i<done.size() ;i++ )
                 ui->doneList->addItem(done.at(i).toLocal8Bit().constData());




        }else{
           QMessageBox::information(this,"Error","Cannot Retrieve Your Task!");
        }

    }
    else
    {
        QMessageBox::information(this,"Error!","DataBase Error");
    }







database.close();





}

AppWindow::~AppWindow()
{

  QListWidgetItem *ItemProgress  =   ui->inProgressList->item(0);
  QListWidgetItem *ItemDone  =   ui->doneList->item(0);
  QListWidgetItem *ItemTodo  =   ui->toDoList->item(0);
  QString inProgress = "";
  QString Done ="";
  QString toDo = "";
    int i= 0, j =0 , z= 0  ;
  while(ItemProgress != nullptr)
  {
      inProgress+=ItemProgress->text()+("&7&");
      i++;
      ItemProgress = ui->inProgressList->item(i);
  }
  while(ItemDone != nullptr)
  {
      Done+=ItemDone->text()+("&7&");
      j++;
      ItemDone = ui->doneList->item(j);
  }
  while(ItemTodo != nullptr)
  {
      toDo+=ItemTodo->text()+("&7&");
      z++;
      ItemTodo = ui->toDoList->item(z);
  }

   QSqlDatabase   database  =  QSqlDatabase::addDatabase("QSQLITE");
    database.setDatabaseName("C:\\Users\\Windows.10\\Documents\\TaskManagement\\user.db");
    if (database.open())
       {
        QSqlQuery query(database);
        query.prepare(QString("UPDATE userdb SET Todo = :toDo  ,Inprogress = :inProgress ,done = :Done WHERE usrname = :username"));
        query.bindValue(":username",username);
        query.bindValue(":toDo",toDo);
        query.bindValue(":Done",Done);
        query.bindValue(":inProgress",inProgress);

      if (!query.exec())
        {
          QMessageBox::information(this,"Error!","Cannot save this session!");
      }


    }
    else
    {
        QMessageBox::information(this,"Error!","DataBase Error");
    }


    database.close();
    delete ui;
}

void AppWindow::on_pushButtonAddTask_clicked()
{
    ui->addTaskLabel->setVisible(1);
    ui->lineEditAddTask->setVisible(1);
    ui->pushButtonCancelAdd->setVisible(1);
    ui->pushButtonVerFaDD->setVisible(1);

}

void AppWindow::on_pushButtonDeleteTask_clicked()
{
   QModelIndex oIndex2 = ui->doneList->currentIndex();
    QModelIndex oIndex3 = ui->inProgressList->currentIndex();
    QModelIndex oIndex1 = ui->toDoList->currentIndex();
       ui->toDoList->model()->removeRow(oIndex1.row());
        ui->doneList->model()->removeRow(oIndex2.row());
        ui->inProgressList->model()->removeRow(oIndex3.row());

}

void AppWindow::on_pushButtonCancelAdd_clicked()
{
    ui->addTaskLabel->hide();
    ui->lineEditAddTask->hide();
    ui->pushButtonCancelAdd->hide();
    ui->pushButtonVerFaDD->hide();
    ui->lineEditAddTask->clear();
}

void AppWindow::on_pushButtonVerFaDD_clicked()
{
    ui->toDoList->addItem(ui->lineEditAddTask->text());
    ui->lineEditAddTask->clear();

}

void AppWindow::on_pushButtonlogOut_clicked()
{
    MainApp *frm = new MainApp(this);
    frm->show();
    this->hide();
}
