#ifndef APPWINDOW_H
#define APPWINDOW_H

#include <QMainWindow>

namespace Ui {
class AppWindow;
}

class AppWindow : public QMainWindow
{
    Q_OBJECT

public:
    QString username = "";
    explicit AppWindow(QWidget *parent = nullptr,QString UserName = "");
    ~AppWindow();

private slots:
    void on_pushButtonAddTask_clicked();
    void on_pushButtonDeleteTask_clicked();
    void on_pushButtonCancelAdd_clicked();
    void on_pushButtonVerFaDD_clicked();

    void on_pushButtonlogOut_clicked();

private:
    Ui::AppWindow *ui;

};

#endif // APPWINDOW_H
