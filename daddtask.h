#ifndef DADDTASK_H
#define DADDTASK_H

#include <QDialog>

namespace Ui {
class dAddTask;
}

class dAddTask : public QDialog
{
    Q_OBJECT

public:
    explicit dAddTask(QWidget *parent = nullptr);
    ~dAddTask();
    bool result  = 1;
    QString task = "";
private slots:
    void on_cancel_dialog_clicked();

    void on_Ok_dialog_clicked();

private:
    Ui::dAddTask *ui;
};

#endif // DADDTASK_H
